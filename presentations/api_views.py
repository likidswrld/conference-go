from django.http import JsonResponse
from common.json import ModelEncoder
from .models import Presentation
from events.models import Conference
import json
from django.views.decorators.http import require_http_methods


class PresentationListEncoder(ModelEncoder):
    model = Presentation
    properties = [
        "presenter_name",
        "company_name",
        "presenter_emai",
        "title",
        "synopsis",
        "created",
        "conference",
    ]


class PresentationDetailEncoder(ModelEncoder):
    model = Presentation
    properties = [
        "presenter_name",
        "company_name",
        "presenter_email",
        "title",
        "synopsis",
        "created",
    ]


@require_http_methods(["GET", "POST"])
def api_list_presentations(request, conference_id):
    if request.method == "GET":
        presentation = Presentation.objects.filter(conference=conference_id)
        return JsonResponse(
            presentation,
            encoder=PresentationListEncoder,
            safe=False,
        )
    else:
        content = json.loads(request.body)
        try:
            conference = Conference.objects.get(id=conference_id)
            content["conference"] = conference
        except Conference.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid conference id"},
                status=400,
            )

    presentation = Presentation.objects.create(**content)
    return JsonResponse(
        Presentation,
        encoder=PresentationDetailEncoder,
        safe=False,
    )


def api_show_presentation(request, id):
    presentation = Presentation.objects.get(id=id)
    return JsonResponse(
        presentation, encoder=PresentationDetailEncoder, safe=False
    )
